package com.example.instagramdownloader.data.model.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ItemThumbnail(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    var img: String,
    var status: String,
    var userName: String,
    var date: String
)