package com.example.instagramdownloader.data.model.network

data class Node(
    val created_at: String,
    val text: String
)