package com.example.instagramdownloader.ui.myfile

import android.os.Bundle
import android.text.format.DateUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.instagramdownloader.data.model.entity.ItemGroupThumbnail
import com.example.instagramdownloader.databinding.FragmentMyFileBinding
import com.example.instagramdownloader.ui.myfile.adapter.GroupThumbnailAdapter

class MyFile : Fragment() {
    private lateinit var binding: FragmentMyFileBinding
    private val vm: MyFileViewModel by viewModels()
    private var groupThumbnails: MutableList<ItemGroupThumbnail> = mutableListOf()
    private lateinit var adapter: GroupThumbnailAdapter
    private var isRefresh: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMyFileBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm.getThumbnailFromDb(requireContext())
        initEvent()
        initView()
    }

    private fun initView() {
        adapter = GroupThumbnailAdapter(groupThumbnails)
        val layoutManager = LinearLayoutManager(
            requireContext(),
            LinearLayoutManager.VERTICAL, false
        )
        binding.rvMyFile.adapter = adapter
        binding.rvMyFile.layoutManager = layoutManager
    }

    private fun initEvent() {
        binding.swRefMyFile.setOnRefreshListener {
            vm.getThumbnailFromDb(requireContext())
        }
        vm.mapGroupThumbnail.observe(viewLifecycleOwner) {
            if (binding.swRefMyFile.isRefreshing) {
                binding.swRefMyFile.isRefreshing = false
                groupThumbnails.clear()
            }
            it.forEach { itemThumbnailMap ->
                groupThumbnails.add(
                    ItemGroupThumbnail(
                        itemThumbnailMap.key,
                        itemThumbnailMap.value
                    )
                )
            }
            adapter.notifyDataSetChanged()
        }

        vm.isLoading.observe(viewLifecycleOwner) {
            if (it) {
                binding.constraintMyFile.alpha = 0.5f
                binding.prgBarMyFile.visibility = View.VISIBLE
            } else {
                binding.constraintMyFile.alpha = 1f
                binding.prgBarMyFile.visibility = View.GONE
            }
        }
    }
}