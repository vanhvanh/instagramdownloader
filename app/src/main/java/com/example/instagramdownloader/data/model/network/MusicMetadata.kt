package com.example.instagramdownloader.data.model.network

data class MusicMetadata(
    val audio_type: Any,
    val music_canonical_id: String,
    val music_info: Any,
    val original_sound_info: Any,
    val pinned_media_ids: Any
)