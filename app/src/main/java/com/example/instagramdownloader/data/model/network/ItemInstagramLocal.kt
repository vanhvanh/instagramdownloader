package com.example.instagramdownloader.data.model.network

data class ItemInstagramLocal(
    val auto_load_more_enabled: Boolean,
    val items: List<Item>,
    val more_available: Boolean,
    val num_results: Int,
    val showQRModal: Boolean
)