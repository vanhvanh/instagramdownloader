package com.example.instagramdownloader.data.local.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.instagramdownloader.data.model.entity.ItemThumbnail

@Database(entities = [ItemThumbnail::class], version = 1)
abstract class DownInsDb : RoomDatabase() {

    companion object {
        const val DATABASE_NAME = "download_ins.db"
        private var instance: DownInsDb? = null
        fun getInstance(context: Context): DownInsDb {
            if (instance == null) {
                instance =
                    Room.databaseBuilder(context, DownInsDb::class.java, DATABASE_NAME).build()
            }
            return instance as DownInsDb
        }
    }

    abstract fun downInsDAO(): DownInsDAO
}