package com.example.instagramdownloader.data.model.network

data class EdgeMediaToTaggedUser(
    val edges: List<Any>
)