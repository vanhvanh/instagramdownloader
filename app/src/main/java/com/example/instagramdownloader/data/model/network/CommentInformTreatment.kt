package com.example.instagramdownloader.data.model.network

data class CommentInformTreatment(
    val action_type: Any,
    val should_have_inform_treatment: Boolean,
    val text: String,
    val url: Any
)