package com.example.instagramdownloader.data.model.network

data class SharingFrictionInfo(
    val bloks_app_url: Any,
    val should_have_sharing_friction: Boolean
)