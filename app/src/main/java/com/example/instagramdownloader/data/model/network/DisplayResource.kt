package com.example.instagramdownloader.data.model.network

data class DisplayResource(
    val config_height: Int,
    val config_width: Int,
    val src: String
)