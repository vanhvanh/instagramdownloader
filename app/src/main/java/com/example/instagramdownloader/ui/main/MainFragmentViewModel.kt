package com.example.instagramdownloader.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainFragmentViewModel : ViewModel() {
    var isLoading : MutableLiveData<Boolean> = MutableLiveData<Boolean>()
    var message : MutableLiveData<String> = MutableLiveData<String>()


}