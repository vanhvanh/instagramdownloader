package com.example.instagramdownloader.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.example.instagramdownloader.R
import com.example.instagramdownloader.databinding.FragmentMainBinding
import com.example.instagramdownloader.ui.home.HomeFragment
import com.example.instagramdownloader.ui.main.adapter.ViewPagerAdapterMain
import com.example.instagramdownloader.ui.myfile.MyFile

class MainFragment : Fragment() {
    private lateinit var binding: FragmentMainBinding
    private val viewPagerAdapterMain by lazy {
        ViewPagerAdapterMain(
            requireActivity(),
            listOf(HomeFragment(), MyFile())
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMainBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.vp2MainFrag.adapter = viewPagerAdapterMain

        initEvent()
    }

    private fun initEvent() {
        binding.navBottomMainFrag.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.menuIconHome -> {
                    binding.vp2MainFrag.setCurrentItem(0, true)
                    true
                }
                R.id.menuIconMyFile -> {
                    binding.vp2MainFrag.setCurrentItem(1, true)
                    true
                }
                else -> {
                    false
                }
            }
        }

        binding.vp2MainFrag.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback(){
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                when(position){
                    0 -> binding.navBottomMainFrag.selectedItemId = R.id.menuIconHome
                    1 -> binding.navBottomMainFrag.selectedItemId = R.id.menuIconMyFile
                }
            }
        })
    }
}