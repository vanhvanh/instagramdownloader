package com.example.instagramdownloader.data.model.network

data class Dimensions(
    val height: Int,
    val width: Int
)