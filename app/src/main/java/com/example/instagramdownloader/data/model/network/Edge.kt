package com.example.instagramdownloader.data.model.network

data class Edge(
    val node: Node
)