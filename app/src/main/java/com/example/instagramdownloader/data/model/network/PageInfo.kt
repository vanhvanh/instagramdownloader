package com.example.instagramdownloader.data.model.network

data class PageInfo(
    val end_cursor: Any,
    val has_next_page: Boolean
)