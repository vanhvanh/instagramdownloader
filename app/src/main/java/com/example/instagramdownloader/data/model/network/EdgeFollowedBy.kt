package com.example.instagramdownloader.data.model.network

data class EdgeFollowedBy(
    val count: Int
)