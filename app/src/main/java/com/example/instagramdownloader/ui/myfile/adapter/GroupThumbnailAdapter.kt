package com.example.instagramdownloader.ui.myfile.adapter

import android.annotation.SuppressLint
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.instagramdownloader.data.model.entity.ItemGroupThumbnail
import com.example.instagramdownloader.databinding.ItemGroupThumbnailBinding
import java.text.SimpleDateFormat
import java.util.*

class GroupThumbnailAdapter(private val groupThumbnails: List<ItemGroupThumbnail>) :
    RecyclerView.Adapter<GroupThumbnailAdapter.GroupThumbnailViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GroupThumbnailViewHolder {
        return GroupThumbnailViewHolder(
            ItemGroupThumbnailBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: GroupThumbnailViewHolder, position: Int) {
        val item = groupThumbnails[position]
        holder.bind(item)
    }

    override fun getItemCount(): Int = groupThumbnails.size

    class GroupThumbnailViewHolder(private val binding: ItemGroupThumbnailBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SimpleDateFormat", "SetTextI18n")
        fun bind(itemGroupThumbnail: ItemGroupThumbnail) {
            val dateGroup = itemGroupThumbnail.dateGroup
            val sdf = SimpleDateFormat("dd/MM/yyy")
            val date: Date = sdf.parse(dateGroup) as Date
            val millis: Long = date.time
            if (DateUtils.isToday(millis)) {
                binding.tvDateGroup.text = "Today"
            } else {
                binding.tvDateGroup.text = dateGroup
            }

            val thumbnailAdapter = ThumbnailAdapter(itemGroupThumbnail.items)
            val layoutManager = LinearLayoutManager(
                binding.root.context,
                LinearLayoutManager.VERTICAL, false
            )
            binding.rvItemThumbnailEntity.adapter = thumbnailAdapter
            binding.rvItemThumbnailEntity.layoutManager = layoutManager
        }
    }
}