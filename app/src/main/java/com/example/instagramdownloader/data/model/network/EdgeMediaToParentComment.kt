package com.example.instagramdownloader.data.model.network

data class EdgeMediaToParentComment(
    val count: Int,
    val edges: List<Any>,
    val page_info: PageInfo
)