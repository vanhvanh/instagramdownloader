package com.example.instagramdownloader.ui.home

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.instagramdownloader.R
import com.example.instagramdownloader.data.local.db.DownInsDb
import com.example.instagramdownloader.data.model.entity.ItemThumbnail
import com.example.instagramdownloader.data.model.network.ItemInstagram
import com.example.instagramdownloader.data.model.network.ItemInstagramLocal
import com.example.instagramdownloader.data.remote.ApiBuilder
import com.example.instagramdownloader.data.remote.IApiRepository
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

class HomeFragmentViewModel : ViewModel() {
    var isLoading: MutableLiveData<Boolean> = MutableLiveData<Boolean>()
    var message: MutableLiveData<String> = MutableLiveData<String>()
    var checkLink: MutableLiveData<Boolean> = MutableLiveData<Boolean>()
    var itemInstagram: MutableLiveData<ItemInstagram> = MutableLiveData<ItemInstagram>()
    var itemInstagramLocal: MutableLiveData<ItemInstagramLocal> =
        MutableLiveData<ItemInstagramLocal>()
    var isSaveToDb: MutableLiveData<Boolean> = MutableLiveData()
    fun getLink(targetUrl: String) {
        viewModelScope.launch(Dispatchers.IO) {
            isLoading.postValue(true)
            val check = urlExists(targetUrl)
            withContext(Dispatchers.Main) {
                isLoading.value = false
                checkLink.value = check
            }
        }
    }

    fun downloadThumbnail(link: String, context: Context) {
        viewModelScope.launch(Dispatchers.IO) {
            isLoading.postValue(true)
            val apiRepository = ApiBuilder.getInstance(context).create(IApiRepository::class.java)
                .downloadThumbnail(link)
            withContext(Dispatchers.Main) {
                isLoading.value = false
                Log.e("vanh", "apiResCode:${apiRepository.code()}")
                Log.e("vanh", "apiResMess:${apiRepository.message()}")
                Log.e("vanh", "apiResBody:${apiRepository.body()}")
                if (apiRepository.body() != null) {
                    itemInstagram.value = apiRepository.body()
                } else {
                    message.postValue("Load data from json local")
                    val itemJsonInsLocal = readText(context, R.raw.data)
                    itemInstagramLocal.postValue(
                        Gson().fromJson(
                            itemJsonInsLocal,
                            ItemInstagramLocal::class.java
                        )
                    )
                }
            }
        }
    }

    fun insertThumbnailToDb(context: Context, itemThumbnail: ItemThumbnail) {
        viewModelScope.launch(Dispatchers.IO) {
            isLoading.postValue(true)
            val idInserted = DownInsDb.getInstance(context).downInsDAO().insertToDb(itemThumbnail)
            withContext(Dispatchers.Main) {
                isLoading.postValue(false)
                if (idInserted > 0) {
                    isSaveToDb.value = true
                    //message.value = "Save Done"
                } else {
                    message.value = "Save Error"
                }
            }
        }
    }

//    private fun getJsonDataFromAsset(context: Context, fileName: String): String? {
//        val jsonString: String
//        try {
//            jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
//        } catch (ioException: IOException) {
//            ioException.printStackTrace()
//            return null
//        }
//        return jsonString
//    }

    @Throws(IOException::class)
    private fun readText(context: Context, resId: Int): String? {
        val inputStream = context.resources.openRawResource(resId)
        val br = BufferedReader(InputStreamReader(inputStream))
        val sb = StringBuilder()
        var s: String? = null
        while (br.readLine().also { s = it } != null) {
            sb.append(s)
            sb.append("\n")
        }
        return sb.toString()
    }

    private fun urlExists(targetUrl: String?): Boolean {
        val urlConnection: HttpURLConnection
        return try {
            Log.e("vanh", "in try")
            urlConnection = URL(targetUrl).openConnection() as HttpURLConnection
            urlConnection.requestMethod = "HEAD"
            // Set timeouts 2000 in milliseconds and throw exception
//            urlConnection.connectTimeout = 2000
//            urlConnection.readTimeout = 2000
            urlConnection.connectTimeout = 4000;
            urlConnection.readTimeout = 4000;
            /* Set timeouts 4000 in milliseconds and it should work as the url
             should return back in 3 seconds.
             httpUrlConn.setConnectTimeout(4000);
             httpUrlConn.setReadTimeout(4000);
             */
            Log.e("vanh", "resCode:${urlConnection.responseCode}")
            urlConnection.responseCode == HttpURLConnection.HTTP_OK
        } catch (e: Exception) {
            Log.e("vanh", e.message.toString())
            false
        }
    }
}