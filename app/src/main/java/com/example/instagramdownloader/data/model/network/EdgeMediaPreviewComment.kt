package com.example.instagramdownloader.data.model.network

data class EdgeMediaPreviewComment(
    val count: Int,
    val edges: List<Any>
)