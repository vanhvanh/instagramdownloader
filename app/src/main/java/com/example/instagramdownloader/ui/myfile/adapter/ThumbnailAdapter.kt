package com.example.instagramdownloader.ui.myfile.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.instagramdownloader.R
import com.example.instagramdownloader.data.model.entity.ItemThumbnail
import com.example.instagramdownloader.databinding.ItemThumbnailBinding

class ThumbnailAdapter(private val thumbnails: List<ItemThumbnail>) :
    RecyclerView.Adapter<ThumbnailAdapter.ThumbnailViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ThumbnailViewHolder {
        return ThumbnailViewHolder(
            ItemThumbnailBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ThumbnailViewHolder, position: Int) {
        val item = thumbnails[position]
        holder.bind(item)
    }

    override fun getItemCount(): Int = thumbnails.size

    class ThumbnailViewHolder(private val binding: ItemThumbnailBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(item: ItemThumbnail) {
            Glide.with(binding.root).load(item.img).placeholder(R.drawable.img_loading)
                .into(binding.ivAvaItemThumbnail)
            binding.tvStatusItemThumbnail.text = item.status
            binding.tvNameUserItemThumbnail.text = "@${item.userName}"
            binding.tvFileNameItemThumbnail.text = "${item.userName}${item.id}.jpg"
        }
    }
}