package com.example.instagramdownloader.data.remote

import com.example.instagramdownloader.data.model.network.ItemInstagram
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface IApiRepository {
    @GET("p/{link}?__a=1&__d=dis")
    suspend fun downloadThumbnail(
        @Path("link") link: String
    ): Response<ItemInstagram>
}