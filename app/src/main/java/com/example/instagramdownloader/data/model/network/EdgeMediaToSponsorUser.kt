package com.example.instagramdownloader.data.model.network

data class EdgeMediaToSponsorUser(
    val edges: List<Any>
)