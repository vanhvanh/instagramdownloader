package com.example.instagramdownloader.data.model.network

data class Candidate(
    val height: Int,
    val url: String,
    val width: Int
)