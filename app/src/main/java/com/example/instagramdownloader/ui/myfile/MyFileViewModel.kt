package com.example.instagramdownloader.ui.myfile

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.instagramdownloader.data.local.db.DownInsDb
import com.example.instagramdownloader.data.model.entity.ItemThumbnail
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MyFileViewModel : ViewModel() {
    var isLoading: MutableLiveData<Boolean> = MutableLiveData<Boolean>()
    var message: MutableLiveData<String> = MutableLiveData<String>()
    var mapGroupThumbnail: MutableLiveData<Map<String, List<ItemThumbnail>>> = MutableLiveData()

    fun getThumbnailFromDb(context: Context) {
        viewModelScope.launch(Dispatchers.IO) {
            isLoading.postValue(true)
            val itemThumbnails = DownInsDb.getInstance(context).downInsDAO().getThumbnailFromDb()
            val currGroup = ""
            val data = itemThumbnails.groupBy { if (it.date == currGroup) currGroup else it.date }
            withContext(Dispatchers.Main){
                isLoading.postValue(false)
                if (data.isNotEmpty()){
                    mapGroupThumbnail.value = data
                }else{
                    message.value = "Data from room db null"
                }
            }
        }
    }
}