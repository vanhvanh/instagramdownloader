package com.example.instagramdownloader.data.model.entity

data class ItemGroupThumbnail(
    val dateGroup: String,
    val items: List<ItemThumbnail>
)