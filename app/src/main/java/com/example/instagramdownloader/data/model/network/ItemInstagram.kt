package com.example.instagramdownloader.data.model.network

data class ItemInstagram(
    val graphql: Graphql,
    val showQRModal: Boolean
)