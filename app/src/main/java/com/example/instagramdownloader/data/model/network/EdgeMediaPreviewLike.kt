package com.example.instagramdownloader.data.model.network

data class EdgeMediaPreviewLike(
    val count: Int,
    val edges: List<Any>
)