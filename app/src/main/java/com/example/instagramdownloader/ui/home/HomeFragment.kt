package com.example.instagramdownloader.ui.home

import android.R.attr.*
import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.MutableLiveData
import com.bumptech.glide.Glide
import com.example.instagramdownloader.R
import com.example.instagramdownloader.data.model.entity.ItemThumbnail
import com.example.instagramdownloader.databinding.FragmentHomeBinding
import com.example.instagramdownloader.utils.AppConstant
import com.example.instagramdownloader.utils.AppUtils
import java.text.SimpleDateFormat
import java.util.*


class HomeFragment : Fragment() {
    private lateinit var binding: FragmentHomeBinding
    private lateinit var itemThumbnail: ItemThumbnail
    private var typeStatusButton: MutableLiveData<Int> = MutableLiveData()
    private val vm: HomeFragmentViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initEvent()
    }

    private fun setTypeButton(
        marginHorizontalDp: Float,
        paddingHorizontalDp: Float,
        text: Int,
        iconLeft: Int
    ) {
        val parameter =
            binding.btnStatusDownload.layoutParams as ConstraintLayout.LayoutParams
        parameter.setMargins(
            AppUtils.pxFromDp(requireContext(), marginHorizontalDp).toInt(),
            parameter.topMargin,
            AppUtils.pxFromDp(requireContext(), marginHorizontalDp).toInt(),
            parameter.bottomMargin
        )
        binding.btnStatusDownload.layoutParams = parameter
        binding.btnStatusDownload.setPadding(
            AppUtils.pxFromDp(requireContext(), paddingHorizontalDp).toInt(),
            0,
            AppUtils.pxFromDp(requireContext(), paddingHorizontalDp).toInt(),
            0
        )
        binding.btnStatusDownload.text = getString(text)
        binding.btnStatusDownload.setCompoundDrawablesWithIntrinsicBounds(
            iconLeft,
            0,
            0,
            0
        )
    }

    @SuppressLint("SetTextI18n")
    private fun initEvent() {
        typeStatusButton.observe(viewLifecycleOwner) {
            if (it == AppConstant.TYPE_DOWNLOAD) {
                setTypeButton(
                    28f, 48f,
                    R.string.str_download_thumbnail_jpg, R.drawable.ic_camera
                )
                binding.tvNotificationStatusDown.visibility = View.GONE
            } else if (it == AppConstant.TYPE_DOWNLOAD_DONE) {
                setTypeButton(
                    115f, 32f,
                    R.string.str_done, R.drawable.ic_double_tick
                )
                binding.tvNotificationStatusDown.visibility = View.VISIBLE
            }
        }
        binding.btnPaste.setOnClickListener {
            vm.getLink(binding.edtPasteLink.text.toString().trim())
        }

        binding.btnDownload.setOnClickListener {
            val parts: List<String> = binding.edtPasteLink.text.toString().trim().split("p/")
            if (parts.size > 1) {
                vm.downloadThumbnail(parts[1].trim(), requireContext())
            } else {
                Toast.makeText(requireContext(), "Load link default", Toast.LENGTH_SHORT).show()
                vm.downloadThumbnail("CfWfj-qPxjX", requireContext())
            }
        }

        binding.edtPasteLink.doAfterTextChanged {
            binding.btnDownload.isEnabled = false
            binding.btnDownload.alpha = 0.5f
            binding.constraintInformationDownload.visibility = View.GONE
            typeStatusButton.value = AppConstant.TYPE_DOWNLOAD
        }

        binding.btnStatusDownload.setOnClickListener {
            if (typeStatusButton.value == AppConstant.TYPE_DOWNLOAD) {
                vm.insertThumbnailToDb(requireContext(), itemThumbnail)
            } else if (typeStatusButton.value == AppConstant.TYPE_DOWNLOAD_DONE) {
                Toast.makeText(
                    requireContext(),
                    "Download Done. Please check my file",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        vm.isSaveToDb.observe(viewLifecycleOwner) {
            if (it) {
                typeStatusButton.value = AppConstant.TYPE_DOWNLOAD_DONE
            }
        }

        // data get retrofit
        vm.itemInstagram.observe(viewLifecycleOwner) { itemInstagram ->
            val currentDate: String =
                SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(Date())
            itemThumbnail = ItemThumbnail(
                img = itemInstagram.graphql.shortcode_media.display_url,
                userName = itemInstagram.graphql.shortcode_media.owner.username,
                status = itemInstagram.graphql.shortcode_media.edge_media_to_caption.edges[0].node.text,
                date = currentDate
            )
            Glide.with(requireContext()).load(itemThumbnail.img)
                .placeholder(R.drawable.img_loading)
                .into(binding.ivAvatar)
            binding.tvStatus.text = itemThumbnail.status
            binding.tvNameUser.text = "@${itemThumbnail.userName}"
            binding.constraintInformationDownload.visibility = View.VISIBLE
        }

        // data local
        vm.itemInstagramLocal.observe(viewLifecycleOwner) {
            val currentDate: String =
                SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(Date())
            itemThumbnail = ItemThumbnail(
                img = it.items[0].image_versions2.candidates[0].url,
                userName = it.items[0].user.username,
                status = it.items[0].caption.text,
                date = currentDate
            )

            Glide.with(requireContext()).load(it.items[0].image_versions2.candidates[0].url)
                .placeholder(R.drawable.img_loading)
                .into(binding.ivAvatar)
            binding.tvNameUser.text = "@${it.items[0].user.username}"
            binding.tvStatus.text = it.items[0].caption.text
            binding.constraintInformationDownload.visibility = View.VISIBLE
        }

        vm.message.observe(viewLifecycleOwner) {
            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
        }

        vm.checkLink.observe(viewLifecycleOwner) {
            if (it) {
                binding.btnDownload.isEnabled = true
                binding.btnDownload.alpha = 1f
                binding.tvErrorLink.visibility = View.INVISIBLE
            } else {
                binding.btnDownload.isEnabled = false
                binding.btnDownload.alpha = 0.5f
                binding.tvErrorLink.visibility = View.VISIBLE
            }
        }

        vm.isLoading.observe(viewLifecycleOwner) {
            if (it) {
                binding.prgBarHomeFrag.visibility = View.VISIBLE
                binding.constraintFragHome.alpha = 0.5f
            } else {
                binding.prgBarHomeFrag.visibility = View.GONE
                binding.constraintFragHome.alpha = 1f
            }
        }

    }
}