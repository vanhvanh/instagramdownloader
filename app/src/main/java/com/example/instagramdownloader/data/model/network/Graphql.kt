package com.example.instagramdownloader.data.model.network

data class Graphql(
    val shortcode_media: ShortcodeMedia
)