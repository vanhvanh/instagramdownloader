package com.example.instagramdownloader.data.model.network

data class EdgeMediaToHoistedComment(
    val edges: List<Any>
)