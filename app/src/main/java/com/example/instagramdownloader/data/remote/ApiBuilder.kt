package com.example.instagramdownloader.data.remote

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


object ApiBuilder {
    private var retrofit: Retrofit? = null
    private var gson: Gson? = null
    fun getInstance(context: Context): Retrofit {
        if (retrofit == null) {
            if(gson == null){
                gson = GsonBuilder()
                    .setDateFormat("yyyy-MM-dd HH:mm:ss")
                    .create()
            }
            retrofit = Retrofit.Builder()
                .baseUrl(ApiEndPoint.DOMAIN)
                .addConverterFactory(GsonConverterFactory.create())
                .client(provideOkhttpClient(context))
                .build()
        } else {
            return retrofit as Retrofit
        }
        return retrofit as Retrofit
    }

    private fun provideOkhttpClient(context: Context): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor()
        val parametersInterceptor = Interceptor { chain: Interceptor.Chain ->
            val original: Request = chain.request()
            val originalHttpUrl: HttpUrl = original.url
            val url = originalHttpUrl.newBuilder()
                .build()
            val requestBuilder: Request.Builder = original.newBuilder().url(url)
//            val sharedPreferences: SharedPreferences = context.getSharedPreferences(
//                AppConstant.SHARED_PREFERENCES_NAME,
//                Context.MODE_PRIVATE
//            )
//            if (sharedPreferences.getBoolean(AppConstant.KEY_IS_LOGIN, false)) {
//                requestBuilder.header(
//                    "Authorization",
//                    "Bearer " + sharedPreferences.getString(AppConstant.KEY_ACCESS_TOKEN, "")
//                )
//            }
            val request: Request = requestBuilder.build()
            chain.proceed(request)
        }
        return OkHttpClient().newBuilder()
            .readTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .connectTimeout(10, TimeUnit.SECONDS)
            .addInterceptor(loggingInterceptor)
            .addInterceptor(parametersInterceptor)
            .build()
    }
}