package com.example.instagramdownloader.data.model.network

data class SharingFrictionInfoX(
    val bloks_app_url: Any,
    val sharing_friction_payload: Any,
    val should_have_sharing_friction: Boolean
)