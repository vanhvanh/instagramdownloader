package com.example.instagramdownloader.utils

object AppConstant {
    const val TYPE_DOWNLOAD = 1
    const val TYPE_DOWNLOAD_DONE = 2
}