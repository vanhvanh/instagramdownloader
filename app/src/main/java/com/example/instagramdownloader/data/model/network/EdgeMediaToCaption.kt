package com.example.instagramdownloader.data.model.network

data class EdgeMediaToCaption(
    val edges: List<Edge>
)