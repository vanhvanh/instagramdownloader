package com.example.instagramdownloader.data.model.network

data class ImageVersions2(
    val candidates: List<Candidate>
)