package com.example.instagramdownloader.data.model.network

data class EdgeWebMediaToRelatedMedia(
    val edges: List<Any>
)