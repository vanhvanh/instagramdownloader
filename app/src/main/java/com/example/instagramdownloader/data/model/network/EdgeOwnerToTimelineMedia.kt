package com.example.instagramdownloader.data.model.network

data class EdgeOwnerToTimelineMedia(
    val count: Int
)