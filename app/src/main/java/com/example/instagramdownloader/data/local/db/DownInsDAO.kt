package com.example.instagramdownloader.data.local.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.instagramdownloader.data.model.entity.ItemThumbnail

@Dao
interface DownInsDAO {
    @Query("SELECT * FROM ItemThumbnail order by date DESC")
    fun getThumbnailFromDb(): List<ItemThumbnail>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertToDb(itemThumbnail: ItemThumbnail) : Long
}