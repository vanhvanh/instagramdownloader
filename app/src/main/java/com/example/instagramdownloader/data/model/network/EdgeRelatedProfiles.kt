package com.example.instagramdownloader.data.model.network

data class EdgeRelatedProfiles(
    val edges: List<Any>
)